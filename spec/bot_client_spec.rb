require 'spec_helper'
require 'web_mock'
require 'byebug'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def stub_get_updates(token, message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def stub_get_inline_keyboard_updates(token, message_text, inline_selection)
  body = {
    "ok": true, "result": [{
      "update_id": 866_033_907,
      "callback_query": { "id": '608740940475689651', "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                          "message": {
                            "message_id": 626,
                            "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                            "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                            "date": 1_595_282_006,
                            "text": message_text,
                            "reply_markup": {
                              "inline_keyboard": [
                                [{ "text": 'Jon Snow', "callback_data": '1' }],
                                [{ "text": 'Daenerys Targaryen', "callback_data": '2' }],
                                [{ "text": 'Ned Stark', "callback_data": '3' }]
                              ]
                            }
                          },
                          "chat_instance": '2671782303129352872',
                          "data": inline_selection }
    }]
  }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def stub_send_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_create_user
  body = {
    "id": 19,
    "nombre": 'egutter',
    "email": 'prueba@prueba.com'
  }

  stub_request(:post, "#{ENV['API_URL']}/usuarios")
    .with(
      body: { "nombre": 'egutter',
              "email": 'prueba@prueba.com' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_create_user_existing_email
  body = {
    "error": 'El usuario o email ya existe'
  }

  stub_request(:post, "#{ENV['API_URL']}/usuarios")
    .with(
      body: { "nombre": 'egutter',
              "email": 'prueba@prueba.com' }
    )
    .to_return(status: 400, body: body.to_json, headers: {})
end

def stub_create_user_existing_username
  body = {
    "error": 'El usuario o email ya existe'
  }

  stub_request(:post, "#{ENV['API_URL']}/usuarios")
    .with(
      body: { "nombre": 'egutter',
              "email": 'prueba@prueba.com' }
    )
    .to_return(status: 400, body: body.to_json, headers: {})
end

def stub_send_keyboard_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"}],[{"text":"Daenerys Targaryen","callback_data":"2"}],[{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_find_latest_when_nothing_new
  stub_request(:get, "#{ENV['API_URL']}/novedades/egutter")
    .to_return(status: 200, body: [].to_json, headers: {})
end

def stub_obtener_mas_vistos_cuando_no_se_vio_nada
  stub_request(:get, "#{ENV['API_URL']}/masvistos/semanal")
    .to_return(status: 200, body: [].to_json, headers: {})
end

def stub_find_latest_with_new_stuff
  body = [
    {
      'id': 10,
      'titulo': 'Toy Story 4',
      'descripcion': 'clasicardo 4',
      'genero': 'COMEDIA',
      'tipo': 'pelicula'
    },
    {
      'id': 9,
      'titulo': 'Toy Story 1',
      'descripcion': 'clasicardo 1',
      'genero': 'COMEDIA',
      'tipo': 'pelicula'
    }
  ]

  stub_request(:get, "#{ENV['API_URL']}/novedades/egutter")
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_obtener_mas_vistos_cuando_se_vio_contenido
  body = [
    {
      "veces_visto": 2,
      "contenido": {
        'id': 10,
        'titulo': 'Toy Story 4',
        'descripcion': 'clasicardo 4',
        'genero': 'COMEDIA',
        'tipo': 'pelicula'
      }
    },
    {
      "veces_visto": 3,
      "contenido": {
        'id': 9,
        'titulo': 'Toy Story 1',
        'descripcion': 'clasicardo 1',
        'genero': 'COMEDIA',
        'tipo': 'pelicula'
      }
    }
  ]

  stub_request(:get, "#{ENV['API_URL']}/masvistos/semanal")
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_get_details_from_film
  body = { 'id': 1,
           'titulo': 'Toy Story 4',
           'descripcion': 'clasicardo 4',
           'genero': 'COMEDIA',
           'tipo': 'pelicula' }

  stub_request(:get, "#{ENV['API_URL']}/contenidos/1")
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_ver_lista_vacia
  body = []

  stub_request(:get, "#{ENV['API_URL']}/lista/egutter")
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_ver_lista_con_contenido
  body = [
    {
      'id': 1,
      'titulo': 'Star Wars 3',
      'descripcion': 'Una vuelta imperdible del universo Star Wars!',
      'genero': 'CIENCIA FICCION',
      'tipo': 'pelicula'
    },
    {
      'id': 2,
      'titulo': 'mandalorina',
      'descripcion': 'Una vuelta imperdible del universo Star Wars!',
      'genero': 'CIENCIA FICCION',
      'tipo': 'serie'
    }
  ]

  stub_request(:get, "#{ENV['API_URL']}/lista/egutter")
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_agregar_a_lista_satisfactorio
  body = [
    {
      "id": 1,
      "titulo": 'Star Wars 3',
      "descripcion": 'Una vuelta imperdible del universo Star Wars!',
      "genero": 'CIENCIA FICCION'
    },
    {
      "id": 15,
      "titulo": 'mandalorina',
      "descripcion": 'Una vuelta imperdible del universo Star Wars!',
      "genero": 'CIENCIA FICCION',
      "numero_temporadas": 2,
      "numero_capitulos": 15
    }
  ]

  stub_request(:post, "#{ENV['API_URL']}/lista/egutter")
    .with(
      body: { "id_contenido": 15 }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_marcar_como_visto_exitoso(id_contenido)
  body = [
    {
      "id": 1,
      "titulo": 'prueba peli titulo',
      "descripcion": 'description peli',
      "genero": 'ROMANTICO'
    },
    {
      "id": id_contenido,
      "titulo": 'The Office',
      "descripcion": 'Gran Serie',
      "genero": 'COMEDIA',
      "numero_temporadas": 5,
      "numero_capitulos": 50
    }
  ]

  stub_request(:post, "#{ENV['API_URL']}/visto/egutter")
    .with(
      body: { "id_contenido": id_contenido }
    )
    .to_return(status: 201, body: body.to_json, headers: {})
end

def stub_marcar_como_visto_capitulo_exitoso(id_contenido, num_capitulo)
  body = [
    {
      "id": 1,
      "titulo": 'prueba peli titulo',
      "descripcion": 'description peli',
      "genero": 'ROMANTICO'
    },
    {
      "id": id_contenido,
      "titulo": 'The Office',
      "descripcion": 'Gran Serie',
      "genero": 'COMEDIA',
      "numero_temporadas": 5,
      "numero_capitulos": 50,
      "capitulos_vistos": [
        num_capitulo
      ]
    }
  ]

  stub_request(:post, "#{ENV['API_URL']}/visto/egutter")
    .with(
      body: { "id_contenido": id_contenido, "numero_capitulo": num_capitulo }
    )
    .to_return(status: 201, body: body.to_json, headers: {})
end

def stub_marcar_como_visto_ya_visto(id_contenido)
  body = {
    "error": 'este contenido ya estaba marcado como visto!'
  }

  stub_request(:post, "#{ENV['API_URL']}/visto/egutter")
    .with(
      body: { "id_contenido": id_contenido }
    )
    .to_return(status: 400, body: body.to_json, headers: {})
end

def stub_marcar_como_visto_contenido_inexistente(id_contenido)
  body = {
    "error": "no se encontro el contenido con el ID ##{id_contenido}"
  }

  stub_request(:post, "#{ENV['API_URL']}/visto/egutter")
    .with(
      body: { "id_contenido": id_contenido }
    )
    .to_return(status: 400, body: body.to_json, headers: {})
end

def stub_marcar_me_gusta_exitoso(id_contenido)
  body = [
    {
      "id": 1,
      "titulo": 'prueba peli titulo',
      "descripcion": 'description peli',
      "genero": 'ROMANTICO'
    },
    {
      "id": id_contenido,
      "titulo": 'The Office',
      "descripcion": 'Gran Serie',
      "genero": 'COMEDIA',
      "numero_temporadas": 5,
      "numero_capitulos": 50
    }
  ]

  stub_request(:post, "#{ENV['API_URL']}/me_gusta/egutter")
    .with(
      body: { "id_contenido": id_contenido }
    )
    .to_return(status: 201, body: body.to_json, headers: {})
end

def stub_marcar_me_gusta_no_visto(id_contenido)
  body = {
    "error": 'no se puede dar me gusta a contenido no visto por el usuario'
  }

  stub_request(:post, "#{ENV['API_URL']}/me_gusta/egutter")
    .with(
      body: { "id_contenido": id_contenido }
    )
    .to_return(status: 400, body: body.to_json, headers: {})
end

def stub_marcar_me_gusta_contenido_inexistente(id_contenido)
  body = {
    "error": "no se encontro el contenido con el ID ##{id_contenido}"
  }

  stub_request(:post, "#{ENV['API_URL']}/me_gusta/egutter")
    .with(
      body: { "id_contenido": id_contenido }
    )
    .to_return(status: 400, body: body.to_json, headers: {})
end

def stub_marcar_me_gusta_ya_marcado(id_contenido)
  body = {
    "error": 'el contenido ya fue marcado como me gusta'
  }

  stub_request(:post, "#{ENV['API_URL']}/me_gusta/egutter")
    .with(
      body: { "id_contenido": id_contenido }
    )
    .to_return(status: 400, body: body.to_json, headers: {})
end

def stub_obtener_recomendaciones_lluvia
  body = [
    {
      'id': 10,
      'titulo': 'Jurassic Park',
      'descripcion': 'buenarda',
      'genero': 'CIENCIA FICCION',
      'tipo': 'pelicula'
    },
    {
      'id': 9,
      'titulo': 'Enders Game',
      'descripcion': 'plot twist',
      'genero': 'CIENCIA FICCION',
      'tipo': 'pelicula'
    }
  ]

  stub_request(:get, "#{ENV['API_URL']}/recomendaciones/egutter")
    .to_return(status: 200, body: body.to_json, headers: {})
end

def stub_obtener_recomendaciones_vacias
  stub_request(:get, "#{ENV['API_URL']}/recomendaciones/egutter")
    .to_return(status: 200, body: [].to_json, headers: {})
end

token = 'fake_token'

describe 'BotClient' do
  it 'should get a /version message and respond with current version' do
    token = 'fake_token'

    stub_get_updates(token, '/version')
    stub_send_message(token, Version.current)

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /start message and respond with Hola' do
    token = 'fake_token'

    stub_get_updates(token, '/start')
    stub_send_message(token, 'Hola, Emilio!')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get an unknown message message and respond with Do not understand' do
    token = 'fake_token'

    stub_get_updates(token, '/unknown')
    stub_send_message(token, 'Uh? No te entiendo! Me repetis la pregunta?')

    app = BotClient.new(token)

    app.run_once
  end
  # rubocop:disable RSpec/ExampleLength

  it 'should get a /registrar and respond with success message when registration goes ok' do
    token = 'fake_token'
    stub_get_updates(token, '/registrar prueba@prueba.com')
    stub_create_user
    stub_send_message(token, 'Bienvenido egutter')
    app = BotClient.new(token)
    app.run_once
    # rubocop:enable RSpec/ExampleLength
  end

  it 'should get a /registrar and respond with error message when email is missing' do
    token = 'fake_token'
    stub_get_updates(token, '/registrar')
    stub_send_message(token, 'Ups! No entendi eso. Usame como: /registrar email@dominio.com')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /registrar and respond with error message when email already exists' do
    stub_get_updates(token, '/registrar prueba@prueba.com')
    stub_create_user_existing_email
    stub_send_message(token, 'El usuario o email ya existe')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /registrar and respond with error message when username already exists' do
    stub_get_updates(token, '/registrar prueba@prueba.com')
    stub_create_user_existing_username
    stub_send_message(token, 'El usuario o email ya existe')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /novedades and respond with no new content when nothing is available' do
    stub_get_updates(token, '/novedades')
    stub_find_latest_when_nothing_new
    stub_send_message(token, 'No hay nada nuevo :(')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /novedades and respond with new content when something is available' do
    stub_get_updates(token, '/novedades')
    stub_find_latest_with_new_stuff
    stub_send_message(token, "Mira! Lo mas nuevo es: \n #10: Toy Story 4 - COMEDIA\n    Pelicula \n\n #9: Toy Story 1 - COMEDIA\n    Pelicula")
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /guardar 15 and respond with new content added to list succesfully' do
    stub_get_updates(token, '/guardar 15')
    stub_agregar_a_lista_satisfactorio
    stub_send_message(token, 'Se guardo satisfactoriamente el contenido #15 en tu lista')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /milista and respond with all content added to the list' do
    stub_get_updates(token, '/milista')
    stub_ver_lista_con_contenido
    stub_send_message(token, "Encontre lo siguiente en tu lista: \n #1: Star Wars 3 - CIENCIA FICCION\n    Pelicula \n\n #2: mandalorina - CIENCIA FICCION\n    Serie")
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /milista and respond with message when list is empty' do
    stub_get_updates(token, '/milista')
    stub_ver_lista_vacia
    stub_send_message(token, 'No hay contenido en tu lista! Podes agregar contenido con /guardar <#ID>')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /visto 15 and respond with success message when content was marked successfully' do
    stub_get_updates(token, '/visto 15')
    stub_marcar_como_visto_exitoso(15)
    stub_send_message(token, 'Se marco como visto el contenido #15 con exito')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /visto 15 and respond with success message when chapter in serie was marked successfully' do
    stub_get_updates(token, '/visto 15 5')
    stub_marcar_como_visto_capitulo_exitoso(15, 5)
    stub_send_message(token, 'Se marco como visto el capitulo 5 de la serie #15')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /visto 15 and respond with error message when content already seen' do
    stub_get_updates(token, '/visto 15')
    stub_marcar_como_visto_ya_visto(15)
    stub_send_message(token, 'Esta pelicula ya la habías visto!')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /visto 999 and respond with error message when content not found' do
    stub_get_updates(token, '/visto 999')
    stub_marcar_como_visto_contenido_inexistente(999)
    stub_send_message(token, 'El contenido seleccionado no existe! Podes probar de vuelta?')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /visto and respond with error message when no content was specified' do
    stub_get_updates(token, '/visto')
    stub_send_message(token, 'Ups! No entendi eso. Usame como: /visto <#ID del Contenido> [Numero Capitulo]')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /me_gusta 15 and respond with success message when content was liked successfully' do
    stub_get_updates(token, '/megusta 15')
    stub_marcar_me_gusta_exitoso(15)
    stub_send_message(token, 'Se indico exitosamente que te gusta el contenido #15')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /me_gusta 15 and respond with error message when movie was not seen' do
    stub_get_updates(token, '/megusta 15')
    stub_marcar_me_gusta_no_visto(15)
    stub_send_message(token, 'No podes darle me gusta a un contenido que no viste!')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /me_gusta and respond with error message when no content was specified' do
    stub_get_updates(token, '/megusta')
    stub_send_message(token, 'Ups! No entendi eso. Usame como: /megusta <#ID del Contenido>')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /me_gusta 999 and respond with error message when content not found' do
    stub_get_updates(token, '/megusta 999')
    stub_marcar_me_gusta_contenido_inexistente(999)
    stub_send_message(token, 'El contenido seleccionado no existe! Podes probar de vuelta?')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /me_gusta 15 and respond with error message when content was already liked' do
    stub_get_updates(token, '/megusta 15')
    stub_marcar_me_gusta_ya_marcado(15)
    stub_send_message(token, 'Ya habias marcado este contenido con me gusta antes!')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /recomendaciones and respond with Sci-fi or Romance content when weather is "rain"' do
    stub_get_updates(token, '/recomendaciones')
    stub_obtener_recomendaciones_lluvia
    stub_send_message(token, "Estas son nuestras recomendaciones! Te las vas a perder?: \n #10: Jurassic Park - CIENCIA FICCION\n    Pelicula \n\n #9: Enders Game - CIENCIA FICCION\n    Pelicula")
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /recomendaciones and respond with error message when content list is empty' do
    stub_get_updates(token, '/recomendaciones')
    stub_obtener_recomendaciones_vacias
    stub_send_message(token, 'Oh, parece que no tenemos recomendaciones por el momento...')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /detalle and respond with details from the content' do
    stub_get_updates(token, '/detalle 1')
    stub_get_details_from_film
    stub_send_message(token, "Encontre esta info: \n #1 Pelicula: Toy Story 4 - COMEDIA\n    clasicardo 4\n    - - -\n    Director: -\n    Actor principal: -\n    Disponible desde el -> -")
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /masvistos and respond with no new content when nothing was seen this week' do
    stub_get_updates(token, '/masvistos')
    stub_obtener_mas_vistos_cuando_no_se_vio_nada
    stub_send_message(token, 'Ningún contenido fue visto esta semana :(')
    app = BotClient.new(token)
    app.run_once
  end

  it 'should get a /novedades and respond with new content when something was seen this week' do
    stub_get_updates(token, '/masvistos')
    stub_obtener_mas_vistos_cuando_se_vio_contenido
    stub_send_message(token, "Aca tenes lo más visto en la semana! \n Visto 2 veces:\n    #10 - Toy Story 4\n    COMEDIA - Pelicula \n\n Visto 3 veces:\n    #9 - Toy Story 1\n    COMEDIA - Pelicula")
    app = BotClient.new(token)
    app.run_once
  end
end
