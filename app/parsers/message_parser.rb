class MessageParser
  def self.tiene_username?(message)
    !message['from']['username'].to_s.nil?
  end

  def self.username(message)
    message['from']['username'].to_s
  end

  def self.cantidad_palabras(message)
    message.to_s.split.size
  end
end
