class AuthHelper
  @logger = Logger.new(STDOUT)
  def self.obtener_api_key
    api_key = ENV['API_SECRET']
    @logger.info "API_KEY OBTENIDA: #{api_key}"
    api_key
  end
end
