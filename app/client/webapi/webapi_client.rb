require_relative 'auth_helper'
require 'rest-client'
require 'json'

# rubocop: disable Metrics/ClassLength
class WebApiClient
  attr_reader :url
  HTTP_CODE_FORBIDDEN = 403
  HTTP_CODE_BAD_REQ = 400
  HTTP_CODE_NOT_FOUND = 404
  HTTP_4XX_CODES = [HTTP_CODE_BAD_REQ, HTTP_CODE_NOT_FOUND, HTTP_CODE_FORBIDDEN].freeze

  def initialize(url = ENV['API_URL'], log_level = ENV['LOG_LEVEL'])
    @url = url
    @logger = Logger.new(STDOUT)
    @logger.level = log_level.to_i
  end

  def create_user(email, username)
    create_user_payload = { 'nombre' => username, 'email': email }
    @logger.info "Calling POST #{@url}/usuarios with payload: #{create_user_payload}"
    http_response = RestClient.post(url + '/usuarios', create_user_payload.to_json, 'Content-Type': 'application/json', 'X-API-KEY': AuthHelper.obtener_api_key)
    handle_create_user_response(http_response)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling POST #{@url}/usuarios : #{e}"
    handle_create_user_response_error(e)
  end

  def fetch_latest_content(username)
    @logger.info "Calling GET #{@url}/novedades/#{username}"
    http_response = RestClient.get(url + "/novedades/#{username}", 'Content-Type': 'application/json', 'X-API-KEY': AuthHelper.obtener_api_key)
    handle_latest_content_response(http_response)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling GET #{@url}/novedades/#{username} : #{e}"
    handle_latest_content_response_error(e)
  end

  def agregar_a_lista(username, id)
    agregar_a_lista_payload = { 'id_contenido' => id }
    @logger.info "Calling POST #{@url}/lista/#{username}/ with body: #{agregar_a_lista_payload}"
    respuesta_http = RestClient.post(url + "/lista/#{username}", agregar_a_lista_payload.to_json, 'Content-Type': 'application/json', 'X-API-KEY': AuthHelper.obtener_api_key)

    manejar_respuesta_agregar_a_lista(respuesta_http, id)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling POST #{@url}/lista/#{username}/ : #{e}"
    manejar_error_agregar_a_lista(e)
  end

  def ver_lista_de_usuario(username)
    @logger.info "Calling GET #{@url}/lista/#{username}"
    http_response = RestClient.get(url + "/lista/#{username}", 'Content-Type': 'application/json', 'X-API-KEY': AuthHelper.obtener_api_key)
    manejar_respuesta_ver_lista(http_response)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling GET #{@url}/lista/#{username} : #{e}"
    manejar_error_ver_lista(e)
  end

  def marcar_como_visto(usuario, atributos)
    marcar_como_visto_payload = { 'id_contenido' => atributos[0] }
    marcar_como_visto_payload['numero_capitulo'] = atributos[1] if atributos.size == 2
    @logger.info "Calling POST #{@url}/visto/#{usuario} with body: #{marcar_como_visto_payload}"
    respuesta_http = RestClient.post(url + "/visto/#{usuario}", marcar_como_visto_payload.to_json, 'Content-Type': 'application/json', 'X-API-KEY': AuthHelper.obtener_api_key)

    manejar_respuesta_marcar_como_visto(respuesta_http)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling POST #{@url}/visto/#{usuario} : #{e}"
    manejar_error_marcar_como_visto(e)
  end

  def marcar_me_gusta(usuario, id_contenido)
    marcar_me_gusta_payload = { 'id_contenido' => id_contenido }
    @logger.info "Calling POST #{@url}/me_gusta/#{usuario} with body: #{marcar_me_gusta_payload}"
    respuesta_http = RestClient.post(url + "/me_gusta/#{usuario}", marcar_me_gusta_payload.to_json, 'Content-Type': 'application/json', 'X-API-KEY': AuthHelper.obtener_api_key)

    manejar_respuesta_marcar_me_gusta(respuesta_http)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling POST #{@url}/me_gusta/#{usuario} : #{e}"
    manejar_error_marcar_me_gusta(e)
  end

  def obtener_recomendaciones(usuario)
    @logger.info "Calling GET #{@url}/recomendaciones/#{usuario}"
    respuesta_http = RestClient.get(url + "/recomendaciones/#{usuario}", 'Content-Type': 'application/json', 'X-API-KEY': AuthHelper.obtener_api_key)
    manejar_respuesta_recomendaciones(respuesta_http)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling GET #{@url}/recomendaciones/#{usuario} : #{e}"
    manejar_error_obtener_recomendaciones(e)
  end

  def obtener_mas_vistos(usuario)
    @logger.info "Calling GET #{@url}/masvistos/semanal"
    respuesta_http = RestClient.get(url + '/masvistos/semanal', 'Content-Type': 'application/json', 'Nombre-Usuario': usuario, 'X-API-KEY': AuthHelper.obtener_api_key)
    manejar_respuesta_mas_vistos(respuesta_http)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling GET #{@url}/masvistos/semanal : #{e}"
    manejar_error_obtener_mas_vistos(e)
  end

  def ver_detalle_contenido(username, id_contenido)
    @logger.info "Calling GET #{@url}/contenidos/#{id_contenido} for user #{username}"
    http_response = RestClient.get(url + "/contenidos/#{id_contenido}", 'Content-Type': 'application/json', 'Nombre-Usuario': username, 'X-API-KEY': AuthHelper.obtener_api_key)
    manejar_ver_detalle(http_response)
  rescue RestClient::ExceptionWithResponse => e
    @logger.error "Error ocurred calling GET #{@url}/contenidos/#{id_contenido} : #{e}"
    manejar_error_ver_detalle(e)
  end

  private

  def handle_create_user_response(http_response)
    @logger.info "POST #{@url}/usuarios response: #{http_response}"
    body = JSON.parse(http_response.body)
    response = { success: true, message: 'Registro OK', id: body['id'],
                 username: body['nombre'],
                 email: body['email'] }
    @logger.debug "handle_create_user_response() response: #{response}"
    response
  end

  def handle_create_user_response_error(exception_response)
    body = JSON.parse(exception_response.response.body)
    response = { success: false, message: body['error'] }
    @logger.debug "handle_create_user_response_error() response: #{response}"
    response
  end

  def handle_latest_content_response(http_response)
    @logger.info "GET #{@url}/novedades/ response: #{http_response}"
    body = JSON.parse(http_response.body)
    response = { success: true, message: '', body: body }
    @logger.info "handle_latest_content_response() response: #{response}"
    response
  end

  def handle_latest_content_response_error(exception_response)
    body = JSON.parse(exception_response.response.body)
    error_message = body['error']

    response = { success: false, message: error_message }
    @logger.debug "handle_latest_content_response_error() response: #{response}"
    response
  end

  def manejar_respuesta_agregar_a_lista(respuesta_http, _id)
    @logger.info "POST #{@url}/lista/ response: #{respuesta_http}"
    body = JSON.parse(respuesta_http.body)
    response = { success: true, message: '', body: body }
    @logger.debug "manejar_respuesta_agregar_a_lista() respuesta: #{response}"
    response
  end

  def manejar_error_agregar_a_lista(respuesta_error)
    body = JSON.parse(respuesta_error.response.body)
    error_message = body['error']
    # if HTTP_4XX_CODES.include?(respuesta_error.response.code)
    # error_message = 'El contenido seleccionado no existe! Podes probar de vuelta?'\
    #   if respuesta_error.response.code == HTTP_CODE_NOT_FOUND

    response = { success: false, message: error_message }
    @logger.debug "manejar_error_agregar_a_lista() response: #{response}"
    response
  end

  def manejar_respuesta_ver_lista(http_response)
    @logger.info "GET #{@url}/lista/ response: #{http_response}"
    body = JSON.parse(http_response.body)
    response = { success: true, message: '', body: body }
    @logger.info "manejar_respuesta_ver_lista() response: #{response}"
    response
  end

  def manejar_error_ver_lista(exception_response)
    body = JSON.parse(exception_response.response.body)
    mensaje_error = body['error']

    response = { success: false, message: mensaje_error }
    @logger.debug "manejar_error_ver_lista() response: #{response}"
    response
  end

  def manejar_respuesta_marcar_como_visto(respuesta_http)
    @logger.info "POST #{@url}/visto/ response: #{respuesta_http}"
    body = JSON.parse(respuesta_http.body)
    response = { success: true, message: '', body: body }
    @logger.debug "manejar_respuesta_marcar_como_visto() response: #{response}"
    response
  end

  def manejar_error_marcar_como_visto(respuesta_excepcion)
    body = JSON.parse(respuesta_excepcion.response.body)
    mensaje_error = body['error']

    response = { success: false, message: mensaje_error }
    @logger.debug "manejar_error_marcar_como_visto() response: #{response}"
    response
  end

  def manejar_respuesta_marcar_me_gusta(respuesta_http)
    @logger.info "POST #{@url}/me_gusta/ response: #{respuesta_http}"
    body = JSON.parse(respuesta_http.body)
    response = { success: true, message: '', body: body }
    @logger.debug "manejar_respuesta_marcar_me_gusta() response: #{response}"
    response
  end

  def manejar_error_marcar_me_gusta(respuesta_excepcion)
    body = JSON.parse(respuesta_excepcion.response.body)
    mensaje_error = body['error']

    response = { success: false, message: mensaje_error }
    @logger.debug "manejar_error_marcar_me_gusta() response: #{response}"
    response
  end

  def manejar_respuesta_recomendaciones(respuesta_http)
    @logger.info "GET #{@url}/recomendaciones/ response: #{respuesta_http}"
    body = JSON.parse(respuesta_http.body)
    respuesta = { success: true, message: '', body: body }
    @logger.info "manejar_respuesta_recomendaciones() response: #{respuesta}"
    respuesta
  end

  def manejar_error_obtener_recomendaciones(respuesta_excepcion)
    body = JSON.parse(respuesta_excepcion.response.body)
    mensaje_error = body['error']

    response = { success: false, message: mensaje_error }
    @logger.debug "manejar_error_obtener_recomendaciones() response: #{response}"
    response
  end

  def manejar_respuesta_mas_vistos(respuesta_http)
    @logger.info "GET #{@url}/masvistos/semanal response: #{respuesta_http}"
    body = JSON.parse(respuesta_http.body)
    respuesta = { success: true, message: '', body: body }
    @logger.info "manejar_respuesta_mas_vistos() response: #{respuesta}"
    respuesta
  end

  def manejar_error_obtener_mas_vistos(respuesta_excepcion)
    body = JSON.parse(respuesta_excepcion.response.body)
    mensaje_error = body['error']

    response = { success: false, message: mensaje_error }
    @logger.debug "manejar_error_obtener_mas_vistos() response: #{response}"
    response
  end

  def manejar_ver_detalle(http_response)
    @logger.info "GET #{@url}/contenidos/<ID> response: #{http_response}"
    body = JSON.parse(http_response.body)
    response = { success: true, message: '', body: body }
    @logger.info "manejar_ver_detalle() response: #{response}"
    response
  end

  def manejar_error_ver_detalle(exception_response)
    body = JSON.parse(exception_response.response.body)
    error_message = body['error']

    response = { success: false, message: error_message }
    @logger.debug "manejar_error_ver_detalle() response: #{response}"
    response
  end
end
# rubocop: enable Metrics/ClassLength

HARDCODED_USER_LIST = {
  success: true,
  message: '',
  body: [
    {
      'id' => 1,
      'titulo' => 'Star Wars 3',
      'descripcion' => 'Una vuelta imperdible del universo Star Wars!',
      'genero' => 'CIENCIA FICCION'
    },
    {
      'id' => 2,
      'titulo' => 'mandalorina',
      'descripcion' => 'Una vuelta imperdible del universo Star Wars!',
      'genero' => 'CIENCIA FICCION',
      'numero_temporadas' => 2,
      'numero_capitulos' => 15
    }
  ]
}.freeze
