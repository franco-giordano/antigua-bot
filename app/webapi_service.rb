require_relative 'client/webapi/webapi_client'
require_relative 'parsers/message_parser'
require_relative 'formatters/content_formatter'
require 'byebug'
# rubocop: disable Metrics/ClassLength
class WebApiService
  def initialize(log_level = ENV['LOG_LEVEL'])
    @logger = Logger.new(STDOUT)
    @logger.level = log_level.to_i
  end

  def create_user(message, args)
    user_registration_response_message = if MessageParser.cantidad_palabras(message) != 2
                                           'Ups! No entendi eso. Usame como: /registrar email@dominio.com'
                                         else
                                           verify_user(message, args)
                                         end
    user_registration_response_message
  end

  def fetch_latest(_message, username)
    client = WebApiClient.new
    response = client.fetch_latest_content(username)
    return ContentFormatter.error_to_s(response) unless response[:success]

    ContentFormatter.latest_list_to_s(response[:body])
  rescue StandardError
    'Ocurrio un error al buscar las novedades'
  end

  def agregar_a_mi_lista(message, username, id)
    client = WebApiClient.new
    id = id.strip.to_i
    return 'Ups! No entendi eso. Usame como: /guardar <#ID del Contenido>'\
      if MessageParser.cantidad_palabras(message) != 2 || id.zero?

    respuesta = client.agregar_a_lista(username, id)

    return ContentFormatter.agregar_a_mi_lista_to_s(respuesta) unless respuesta[:success]
    return "Se guardo satisfactoriamente el contenido ##{id} en tu lista" if respuesta[:success]

    respuesta[:message]
  rescue StandardError
    'Ocurrio un error al agregar a tu lista'
  end

  def ver_mi_lista(_message, username)
    client = WebApiClient.new
    response = client.ver_lista_de_usuario(username)
    return ContentFormatter.error_list_to_s(response) unless response[:success]

    ContentFormatter.user_list_to_s(response[:body])
  rescue StandardError
    'Ocurrio un error al buscar tu lista'
  end

  def marcar_contenido_como_visto(mensaje, usuario, atributos_para_marcar)
    atributos = atributos_para_marcar.split.map(&:to_i)
    @logger.debug "ATRIBS #{atributos}"
    return 'Ups! No entendi eso. Usame como: /visto <#ID del Contenido> [Numero Capitulo]' unless (2..3).include?(MessageParser.cantidad_palabras(mensaje))
    return 'Ups! No entendi eso. Usame como: /visto <#ID del Contenido> [Numero Capitulo]' unless atributo_con_capitulo_correcto(atributos)

    cliente_api = WebApiClient.new
    respuesta = cliente_api.marcar_como_visto(usuario, atributos)
    ContentFormatter.marcar_como_visto_to_s(respuesta, atributos)
  rescue StandardError
    'Ocurrio un error al marcar como visto'
  end

  def marcar_contenido_me_gusta(mensaje, usuario, id_contenido)
    id = id_contenido.to_i
    return 'Ups! No entendi eso. Usame como: /megusta <#ID del Contenido>' if MessageParser.cantidad_palabras(mensaje) != 2 || id.zero?

    cliente_api = WebApiClient.new
    respuesta = cliente_api.marcar_me_gusta(usuario, id_contenido)
    return ContentFormatter.marcar_me_gusta_to_s(respuesta) unless respuesta[:success]
    return "Se indico exitosamente que te gusta el contenido ##{id_contenido}" if respuesta[:success]
  rescue StandardError
    'Ocurrio un error al marcar como me gusta'
  end

  def ver_detalle_contenido(mensaje, usuario, id_contenido)
    id = id_contenido.to_i
    return 'Ups! No entendi eso. Usame como: /detalle <#ID del Contenido>' if id.zero? || MessageParser.cantidad_palabras(mensaje) != 2

    cliente_api = WebApiClient.new
    respuesta = cliente_api.ver_detalle_contenido(usuario, id)

    return ContentFormatter.ver_detalle_error(respuesta) unless respuesta[:success]

    ContentFormatter.ver_detalle_singular(respuesta[:body])
  rescue StandardError
    'Ocurrio un error al ver detalle de contenido'
  end

  def obtener_recomendaciones(_message, username)
    cliente_api = WebApiClient.new
    respuesta = cliente_api.obtener_recomendaciones(username)
    return ContentFormatter.error_to_s(respuesta) unless respuesta[:success]

    ContentFormatter.recomendaciones_list_to_s(respuesta[:body])
  rescue StandardError
    'Ocurrio un error al obtener recomendaciones'
  end

  def obtener_mas_vistos(_mensaje, usuario)
    cliente_api = WebApiClient.new
    respuesta = cliente_api.obtener_mas_vistos(usuario)
    return ContentFormatter.error_to_s(respuesta) unless respuesta[:success]

    ContentFormatter.mas_vistos_list_to_s(respuesta[:body])
  rescue StandardError
    'Ocurrio un error al obtener los mas vistos de la semana'
  end

  private

  def call_user_registration(email, username)
    client = WebApiClient.new
    response = client.create_user(email, username)
    return "Bienvenido #{username}" if response[:success]
    return response[:message] unless response[:success]
  rescue StandardError
    'Ocurrio un error al registrar'
  end

  def verify_user(message, args)
    if !MessageParser.tiene_username?(message)
      'Por favor, asocia un usuario a tu cuenta de Telegram para poder registrarte'
    else
      email = args['email'].strip # TODO: validacion email
      username = MessageParser.username(message)
      call_user_registration(email, username)
    end
  end

  def atributo_con_capitulo_correcto(atribs)
    return atribs.fetch(1, 0).positive? if atribs.length == 2

    true
  end
end
# rubocop: enable Metrics/ClassLength
