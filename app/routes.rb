require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"
require_relative 'webapi_service'

class Routes
  include Routing

  on_message '/start' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{message.from.first_name}!")
  end

  on_message '/version' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  on_message_pattern %r{/registrar(?<email>.*)} do |bot, message, args|
    service = WebApiService.new
    user_registration_response_message = service.create_user(message, args)

    bot.api.send_message(chat_id: message.chat.id, text: user_registration_response_message)
  end

  on_message '/novedades' do |bot, message|
    service = WebApiService.new
    latest_content = service.fetch_latest(message, message.from.username)

    bot.api.send_message(chat_id: message.chat.id, text: latest_content)
  end

  on_message_pattern %r{/guardar(?<id_contenido>.*)} do |bot, message, args|
    service = WebApiService.new
    mensaje_respuesta_agregar_a_mi_lista = service.agregar_a_mi_lista(message, MessageParser.username(message), args['id_contenido'])

    bot.api.send_message(chat_id: message.chat.id, text: mensaje_respuesta_agregar_a_mi_lista)
  end

  on_message '/milista' do |bot, message|
    service = WebApiService.new
    listado = service.ver_mi_lista(message, MessageParser.username(message))

    bot.api.send_message(chat_id: message.chat.id, text: listado)
  end

  on_message_pattern %r{/visto(?<id_contenido>.*)} do |bot, mensaje, args|
    service = WebApiService.new
    mensaje_respuesta_marcar_como_visto = service.marcar_contenido_como_visto(mensaje, MessageParser.username(mensaje), args['id_contenido'].strip)

    bot.api.send_message(chat_id: mensaje.chat.id, text: mensaje_respuesta_marcar_como_visto)
  end

  on_message_pattern %r{/megusta(?<id_contenido>.*)} do |bot, mensaje, args|
    service = WebApiService.new
    mensaje_respuesta_marcar_me_gusta = service.marcar_contenido_me_gusta(mensaje, MessageParser.username(mensaje), args['id_contenido'].to_i)

    bot.api.send_message(chat_id: mensaje.chat.id, text: mensaje_respuesta_marcar_me_gusta)
  end

  on_message '/recomendaciones' do |bot, message|
    service = WebApiService.new
    recomendaciones = service.obtener_recomendaciones(message, message.from.username)

    bot.api.send_message(chat_id: message.chat.id, text: recomendaciones)
  end

  on_message_pattern %r{/detalle(?<id_contenido>.*)} do |bot, mensaje, args|
    service = WebApiService.new
    mensaje_respuesta_ver_detalle = service.ver_detalle_contenido(mensaje, MessageParser.username(mensaje), args['id_contenido'].to_i)

    bot.api.send_message(chat_id: mensaje.chat.id, text: mensaje_respuesta_ver_detalle)
  end

  on_message '/masvistos' do |bot, mensaje|
    service = WebApiService.new
    masvistos = service.obtener_mas_vistos(mensaje, mensaje.from.username)

    bot.api.send_message(chat_id: mensaje.chat.id, text: masvistos)
  end

  default do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Uh? No te entiendo! Me repetis la pregunta?')
  end
end
