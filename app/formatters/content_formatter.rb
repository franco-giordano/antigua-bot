require 'byebug'

class ContentFormatter
  def self.latest_list_to_s(api_response)
    return 'No hay nada nuevo :(' if api_response.length.zero?

    latest = api_response.clone

    "Mira! Lo mas nuevo es: \n " +
      latest.map { |content| ContentFormatter.content_to_s(content) }.join(" \n\n ")
  end

  def self.content_to_s(api_response)
    "##{api_response['id']}: #{api_response['titulo']} - #{api_response['genero']}
    #{api_response['tipo'].capitalize}"
  end

  def self.mas_visto_to_s(api_response)
    veces = api_response['veces_visto'] > 1 ? 'veces' : 'vez'
    "Visto #{api_response['veces_visto']} #{veces}:
    ##{api_response['contenido']['id']} - #{api_response['contenido']['titulo']}
    #{api_response['contenido']['genero']} - #{api_response['contenido']['tipo'].capitalize}"
  end

  def self.ver_detalle_error(api_response)
    return 'Parece que no estas registrado! Podes hacerlo ingresando: /registrar <email>' if api_response[:message].include? 'usuario no registrado'
    return 'El contenido seleccionado no existe! Podes probar de vuelta?' if api_response[:message].include? 'no se encontro'

    api_response[:message]
  end

  def self.ver_detalle_singular(api_response)
    "Encontre esta info: \n " + ContentFormatter.contenido_entero_to_s(api_response)
  end

  def self.contenido_entero_to_s(api_response)
    return ContentFormatter.peli_entera_to_s(api_response) if api_response['tipo'] == 'pelicula'
    return ContentFormatter.serie_entera_to_s(api_response) if api_response['tipo'] == 'serie'
  end

  def self.peli_entera_to_s(api_response)
    fecha = Date.parse(api_response.fetch('fecha_creacion', '01-01-2000')).strftime('%d/%m/%Y')

    "##{api_response['id']} #{api_response['tipo'].capitalize}: #{api_response['titulo']} - #{api_response['genero']}
    #{api_response['descripcion']}
    #{api_response.fetch('pais', '-')} - #{api_response.fetch('anio_estreno', '-')}
    Director: #{api_response.fetch('director', '-')}
    Actor principal: #{api_response.fetch('actor_principal', '-')}
    Disponible desde el -> #{fecha == '01/01/2000' ? '-' : fecha}"
  end

  # rubocop:disable Metrics/AbcSize
  def self.serie_entera_to_s(api_response)
    temporadas = api_response.fetch('numero_capitulos', 2) > 1 ? 'Temporadas' : 'Temporada'
    capitulos = api_response.fetch('numero_capitulos', 2) > 1 ? 'Capitulos divididos' : 'Capitulo dividido'
    fecha = Date.parse(api_response.fetch('fecha_creacion', '01-01-2000')).strftime('%d/%m/%Y')

    "##{api_response['id']} #{api_response['tipo'].capitalize}: #{api_response['titulo']} - #{api_response['genero']}
    #{api_response['descripcion']}
    #{api_response.fetch('numero_capitulos', '-')} #{capitulos} en #{api_response['numero_temporadas']} #{temporadas}
    #{api_response.fetch('pais', '-')} - #{api_response.fetch('anio_estreno', '-')}
    Director: #{api_response.fetch('director', '-')}
    Actor principal: #{api_response.fetch('actor_principal', '-')}
    Disponible desde el -> #{fecha == '01/01/2000' ? '-' : fecha}"
    # rubocop:enable Metrics/AbcSize
  end

  def self.user_list_to_s(api_response)
    return 'No hay contenido en tu lista! Podes agregar contenido con /guardar <#ID>' if api_response.length.zero?

    lista = api_response.clone

    "Encontre lo siguiente en tu lista: \n " +
      lista.map { |content| ContentFormatter.content_to_s(content) }.join(" \n\n ")
  end

  def self.error_list_to_s(api_response)
    return 'Parece que no estas registrado! Podes hacerlo ingresando: /registrar <email>' if api_response[:message].include? 'usuario no registrado'
  end

  # rubocop: disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
  def self.marcar_como_visto_to_s(api_response, atributos)
    return 'Parece que no estas registrado! Podes hacerlo ingresando: /registrar <email>' if api_response[:message].include? 'usuario no registrado'
    return 'Tenes que especificar un capitulo ;)' if atributos.length == 1 && api_response[:message].include?('El numero de capitulo es un campo obligatorio')
    return 'El capitulo no existe :(' if atributos.length == 2 && api_response[:message].include?('numero de capitulo no existe')
    return 'El contenido seleccionado no existe! Podes probar de vuelta?' if api_response[:message].include? 'no se encontro'
    return 'Este capitulo ya lo habías visto!' if atributos.length == 2 && api_response[:message].include?('ya estaba marcado como visto')
    return 'Esta pelicula ya la habías visto!' if atributos.length == 1 && api_response[:message].include?('ya estaba marcado como visto')
    return "Se marco como visto el contenido ##{atributos[0]} con exito" if api_response[:success] && atributos.length == 1
    return "Se marco como visto el capitulo #{atributos[1]} de la serie ##{atributos[0]}" if api_response[:success] && atributos.length == 2

    api_response[:message]
  end
  # rubocop: enable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity

  def self.marcar_me_gusta_to_s(api_response)
    return 'Parece que no estas registrado! Podes hacerlo ingresando: /registrar <email>' if api_response[:message].include? 'usuario no registrado'
    return 'No podes darle me gusta a un contenido que no viste!' if api_response[:message].include? 'contenido no visto por el usuario'
    return 'El contenido seleccionado no existe! Podes probar de vuelta?' if api_response[:message].include? 'no se encontro'
    return 'Ya habias marcado este contenido con me gusta antes!' if api_response[:message].include? 'ya fue marcado como'

    api_response[:message]
  end

  def self.recomendaciones_list_to_s(api_response)
    return 'Oh, parece que no tenemos recomendaciones por el momento...' if api_response.length.zero?

    recos = api_response.clone

    "Estas son nuestras recomendaciones! Te las vas a perder?: \n " +
      recos.map { |content| ContentFormatter.content_to_s(content) }.join(" \n\n ")
  end

  def self.agregar_a_mi_lista_to_s(api_response)
    return 'Parece que no estas registrado! Podes hacerlo ingresando: /registrar <email>' if api_response[:message].include? 'usuario no registrado'
    return 'El contenido seleccionado no existe! Podes probar de vuelta?' if api_response[:message].include? 'no se encontro'
    return 'Este contenido ya está en tu lista! Que esperás para verlo?' if api_response[:message].include? 'ya esta en tu lista'

    api_response[:message]
  end

  def self.mas_vistos_list_to_s(api_response)
    return 'Ningún contenido fue visto esta semana :(' if api_response.length.zero?

    recos = api_response.clone
    "Aca tenes lo más visto en la semana! \n " +
      recos.map { |content| ContentFormatter.mas_visto_to_s(content) }.join(" \n\n ")
  end

  def self.error_to_s(api_response)
    return 'Parece que no estas registrado! Podes hacerlo ingresando: /registrar <email>' if api_response[:message].include? 'usuario no registrado'

    api_response[:message]
  end
end
